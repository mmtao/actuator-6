#
# Copyright (c) 2024 The Arizona Board of Regents on behalf of the University 
# of Arizona. All rights reserved.
# 
# This file is part of Actuator-6. Actuator-6 is free software: you can 
# redistribute it and/or modify it under the terms of the GNU General Public 
# License as  published by the Free Software Foundation, either version 3 of 
# the License, or  (at your option) any later version. Actuator-6 is distributed
# in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
# implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See 
# the GNU General Public License for more details. You should have received a 
# copy of the GNU General Public License along with Actuator-6. 
# If not, see <https://www.gnu.org/licenses/>. 
#

cmake_minimum_required(VERSION 3.20.0)
find_package(Zephyr REQUIRED HINTS $ENV{ZEPHYR_BASE})
project(blinky)

target_sources(app PRIVATE 
    src/actuator.cc
    src/blob.cc
    src/lz4.c
)

add_custom_target(dis 
    COMMAND arm-zephyr-eabi-objdump -D zephyr/zephyr.elf > zephyr/zephyr.dis 
    DEPENDS zephyr
)
