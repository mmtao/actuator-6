# Actuator-6

This project is a starting point for building software for version 6 of the
actuators that power the MAPS adaptive secondary mirror for the MMT Observatory.
The actuators themselves are a custom board with a STM32F446ZE microcontroller.
This software currently runs on the Nucleo F446RE board, which has the same 
flash configuration as the ZE but a different package and pins. The RE board is 
very cost-effective, so it is a good starting point.

This repository is configured as an in-tree Zephyr OS application.

When it boots on the RE board, the console output looks like this:
```
Version:        6.0.0
Device ID:      001e00493931501820383358
Size in flash:  22062
Image CRC:      0xd91a697b
Pre-lz4 crc:    0x41191de6 (verified)
Post-lz4 crc:   0x1b055991 (verified)
Boot time:      45 ms
```

The final output of `west build applications/actuator-6 -b nucleo_f446re --pristine` is this:
```
Memory region         Used Size  Region Size  %age Used
           FLASH:       22062 B       512 KB      4.21%
             RAM:       77056 B       128 KB     58.79%
     BACKUP_SRAM:          0 GB         4 KB      0.00%
        IDT_LIST:          0 GB        32 KB      0.00%
```

v5 of the actuator software stores the FPGA's binary blob uncompressed in flash. 
Here, I've stored it compressed by LZ4 in flash and decompressed it into RAM as a 
demonstration of the possibilities for future software development. Where RAM
in short supply, one can imagine decompressing the blob in stages and sending them
to the FPGA as they become available. Since the v5 application's RAM usage is 
small, this isn't a high priority. Similarly, the F446 has a hardware CRC 
calculator, but the software implementation in Zephyr seems more than fast enough.