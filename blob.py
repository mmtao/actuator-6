#!/usr/bin/env python3

#
# Copyright (c) 2024 The Arizona Board of Regents on behalf of the University 
# of Arizona. All rights reserved.
# 
# This file is part of Actuator-6. Actuator-6 is free software: you can 
# redistribute it and/or modify it under the terms of the GNU General Public 
# License as  published by the Free Software Foundation, either version 3 of 
# the License, or  (at your option) any later version. Actuator-6 is distributed
# in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
# implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See 
# the GNU General Public License for more details. You should have received a 
# copy of the GNU General Public License along with Actuator-6. 
# If not, see <https://www.gnu.org/licenses/>. 
#

from crccheck.crc import Crc32
import lz4.block
import sys

# Print out the information that we're going to put in C.
print('#include <zephyr/sys/crc.h>')
print('#include <cstdint>')
print('#include <stdio.h>')
print('#include "lz4.h"')
print()

unc_fname = "iCE40.bin"

# Open the uncompressed file.
with open(unc_fname, 'rb') as file:
    uncompressed_data = bytearray(file.read())
    unc_size = len(uncompressed_data)

# Compress the file.
compressed_data = lz4.block.compress(uncompressed_data, mode='high_compression', compression=12)
cmp_size = len(compressed_data)

# Test.
test_data = lz4.block.decompress(compressed_data)
if Crc32.calc(test_data) != Crc32.calc(uncompressed_data):
    print("CRC decompression test failed.")
    sys.exit(-1)

# Put the compressed data in the file.
print("static const uint8_t blob_cmp[] = {")
s = "  "
i = 0
for byte in compressed_data:
    s += "0x%02x, " % byte
    i = i + 1
    if i % 16 == 0:
        s += "\n  "
s += "};\n"
print(s)

# Information about the uncompressed data.
print("static const uint32_t blob_unc_crc = 0x%08x;" % Crc32.calc(uncompressed_data))
print("static const uint32_t blob_unc_size = %d;" % unc_size)
print("static uint8_t blob_unc[%d];" % unc_size)

# Information about the compressed data.
print("static const uint32_t blob_cmp_crc = 0x%08x;" % Crc32.calc(compressed_data))
print("static const uint32_t blob_cmp_size = %d;" % cmp_size)

print("""
void blob_init() {
    // Step 1. CRC the blob in flash.
    uint32_t crc_act = crc32_ieee(blob_cmp, blob_cmp_size);
    if(blob_cmp_crc != crc_act) {
      printf("Blob compressed CRC failed. %08x != %08x.\\n", crc_act, blob_cmp_crc);
    } else {
      printf("Blob compressed CRC verified. (%08x) \\n", crc_act);
    }

    // Step 2. Decompress.
    LZ4_decompress_fast(reinterpret_cast<const char*>(blob_cmp)+4, reinterpret_cast<char*>(blob_unc), blob_unc_size);

    // Step 3. CRC the uncompressed blob.
    crc_act = crc32_ieee(blob_unc, blob_unc_size);
    if(blob_unc_crc != crc_act) {
      printf("Blob uncompressed CRC failed. %08x != %08x.\\n", crc_act, blob_unc_crc);
    } else {
      printf("Blob uncompressed CRC verified. (0x%08x) \\n", crc_act);
    }
}
""")