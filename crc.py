#!/usr/bin/env python3

#
# Copyright (c) 2024 The Arizona Board of Regents on behalf of the University 
# of Arizona. All rights reserved.
# 
# This file is part of Actuator-6. Actuator-6 is free software: you can 
# redistribute it and/or modify it under the terms of the GNU General Public 
# License as  published by the Free Software Foundation, either version 3 of 
# the License, or  (at your option) any later version. Actuator-6 is distributed
# in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
# implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See 
# the GNU General Public License for more details. You should have received a 
# copy of the GNU General Public License along with Actuator-6. 
# If not, see <https://www.gnu.org/licenses/>. 
#

from crccheck.crc import Crc32
import struct
import sys

crc = 0
with open(sys.argv[1], 'rb') as file:
    data = bytearray(file.read())
    crc = Crc32.calc(data)
    print("Computed CRC is %08x." % crc)

# with open(sys.argv[1], 'ab') as file:
#     file.write(struct.pack('I', crc))
