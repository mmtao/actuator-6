/*
 * Copyright (c) 2024 The Arizona Board of Regents on behalf of the University 
 * of Arizona. All rights reserved.
 * 
 * This file is part of Actuator-6. Actuator-6 is free software: you can 
 * redistribute it and/or modify it under the terms of the GNU General Public 
 * License as  published by the Free Software Foundation, either version 3 of 
 * the License, or  (at your option) any later version. Actuator-6 is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See 
 * the GNU General Public License for more details. You should have received a 
 * copy of the GNU General Public License along with Actuator-6. 
 * If not, see <https://www.gnu.org/licenses/>. 
 */

#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <zephyr/kernel.h>
#include <zephyr/sys_clock.h>

#include "blob.h"
#include "led.h"
#include "sys_info.h"
#include "version.h"

static led_t led;
K_THREAD_DEFINE(blink_the_led, 1024, led.run, nullptr, nullptr, nullptr, 2, 0, 1000);

extern "C" int main(void) {
  printf("Version:\t%s\n", ACTUATOR_VERSION);

  cpu_id_t ci;
  printf("Device ID:  \t%s\n", ci.c_str());

  flash_info_t fi;
  printf("Size in flash:  %d\n", fi.getSizeBytes());
  printf("Image CRC:\t0x%08x\n", fi.actualCrc());

  blob_init();

  printf("Boot time:\t%lld ms\n", k_uptime_get());

  return 0;
}
