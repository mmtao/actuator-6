/*
 * Copyright (c) 2024 The Arizona Board of Regents on behalf of the University 
 * of Arizona. All rights reserved.
 * 
 * This file is part of Actuator-6. Actuator-6 is free software: you can 
 * redistribute it and/or modify it under the terms of the GNU General Public 
 * License as  published by the Free Software Foundation, either version 3 of 
 * the License, or  (at your option) any later version. Actuator-6 is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See 
 * the GNU General Public License for more details. You should have received a 
 * copy of the GNU General Public License along with Actuator-6. 
 * If not, see <https://www.gnu.org/licenses/>. 
 */

#pragma once

#include <zephyr/drivers/gpio.h>

/* The devicetree node identifier for the "led0" alias. */
#define LED0_NODE DT_ALIAS(led0)

class led_t {
 private:
  static constexpr struct gpio_dt_spec led = GPIO_DT_SPEC_GET(LED0_NODE, gpios);
  static constexpr uint32_t sleepTimeMs = 500;
  static bool led_state;

 public:
  static void run() {
    led_state = false;

    if (!gpio_is_ready_dt(&led)) {
      return;
    }

    int ret = gpio_pin_configure_dt(&led, GPIO_OUTPUT_ACTIVE);
    if (ret < 0) {
      return;
    }

    while (1) {
      ret = gpio_pin_toggle_dt(&led);
      if (ret < 0) {
        return;
      }
      led_state = !led_state;

      // printf("LED state: %s\n", led_state ? "ON" : "OFF");
      k_msleep(sleepTimeMs);
    }
  }
};

bool led_t::led_state;