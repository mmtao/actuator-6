/*
 * Copyright (c) 2024 The Arizona Board of Regents on behalf of the University 
 * of Arizona. All rights reserved.
 * 
 * This file is part of Actuator-6. Actuator-6 is free software: you can 
 * redistribute it and/or modify it under the terms of the GNU General Public 
 * License as  published by the Free Software Foundation, either version 3 of 
 * the License, or  (at your option) any later version. Actuator-6 is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See 
 * the GNU General Public License for more details. You should have received a 
 * copy of the GNU General Public License along with Actuator-6. 
 * If not, see <https://www.gnu.org/licenses/>. 
 */

#pragma once

#include <stdio.h>
#include <zephyr/sys/crc.h>

#include <cstdint>

class cpu_id_t {
 private:
  uint32_t* p;
  uint32_t w1, w2, w3;
  char str[25];

 public:
  cpu_id_t() {
    p = reinterpret_cast<uint32_t*>(0x1FFF7A10);
    w1 = p[0];
    w2 = p[1];
    w3 = p[2];

    // Make a C-style string representation.
    sprintf(str, "%08x", w1);
    sprintf(str + 8, "%08x", w2);
    sprintf(str + 16, "%08x", w3);
  }
  ~cpu_id_t() {}

  char* c_str() { return str; }
};

extern unsigned int __rom_start_address;
static const uint32_t __attribute__((section(".last_section"))) rom_end = 0xaa55aa55;

class flash_info_t {
 private:
  uint8_t* start = nullptr;
  uint32_t sizeBytes = 0;

 public:
  flash_info_t() {
    uint32_t rom0 = (uint32_t)&__rom_start_address, rom1 = (uint32_t)&rom_end;
    uint8_t *rom2 = (uint8_t*)&rom_end, k = 0;
    for (int i = 0; i < 256; i++) {
      // printf("%02x %02x %02x %02x\n", rom2[0], rom2[1], rom2[2], rom2[3]);
      if (rom2[0] == 0x15 && rom2[1] == 0xe0 && rom2[2] == 0x15 && rom2[3] == 0xe0) {
        break;
      }
      rom2++;
      k++;
    }

    start = reinterpret_cast<uint8_t*>(rom0);
    sizeBytes = rom1 - rom0 + k + 4;
  }
  ~flash_info_t() {}
  uint32_t getSizeBytes() { return sizeBytes; }
  uint8_t* getStart() { return start; }
  uint32_t actualCrc() { return crc32_ieee(getStart(), getSizeBytes()); }
};